# React With Typescript 

This project uses redux, react, react-router, typescript and webpack. Code spliting is enabled via System.import.

##### What is next

  - Sass loader
  - Unit tests

### Installation
```sh
$ npm install
$ typings install
$ npm start
```
